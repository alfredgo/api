var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2jamh/collections/movimientos?apiKey=ta3v7IL8juZy4qv4qukZZ6sV2jMP-CjT";

var clienteMlab = requestjson.createClient(urlmovimientosMlab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get("/movimientos", function(req, res) {
  clienteMlab.get('', function(err, resM, body) {
    if(err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

/*var data = {
  "nombre": "Pablo",
  "apellido": "Marmol"
};
app.post("/movimientos", function(req, res) {
  clienteMlab.post('', data, function(err, resM, body) {
    if(err) {
      console.log(body);
    } else {
      console.log(res.statusCode);
      res.send(body);
    }
  })
})*/

app.post( "/movimientos", function( req, res ) {
 console.log( "BODY: " + req.body );
 clienteMlab.post( '', req.body, function( err, resM, body ){
   if( err ){
     console.log( "ERROR: " + body );
   } else {
     console.log( "OK: " + body );
     res.send( body );
   }
 });
});

app.get("/clientes/:idcliente", function(req, res) {
  res.send("Aqui tiene al cliente numero: " + req.params.idcliente);
})

app.post("/", function(req, res) {
  res.send("Hemos recibido su petición actualizada");
})

app.put("/", function(req, res) {
  res.send("Hemos recibido su petición put");
})

app.delete("/", function(req, res) {
  res.send("Hemos recibido su petición delete");
})
